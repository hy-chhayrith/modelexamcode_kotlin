package com.example.chhayrithhy.modelexamcode.feature

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView

class DefaultListView : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_default_list_view)

        var arrayList = arrayListOf(1, 2, 3, 4)

        var list_view = findViewById<ListView>(R.id.defaultListView)
        list_view.adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, arrayList)
    }
}
