package com.example.chhayrithhy.modelexamcode.feature

data class Student(var id: Int, var name: String, var age: Int)

object studentData{
    var data = listOf(
            Student(1, "A", 10),
            Student(2, "B", 11),
            Student(3, "C", 12),
            Student(4, "D", 14)
    )
}