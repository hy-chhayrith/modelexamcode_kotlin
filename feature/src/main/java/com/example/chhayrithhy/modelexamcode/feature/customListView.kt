package com.example.chhayrithhy.modelexamcode.feature

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ListView
import android.widget.TextView
import org.w3c.dom.Text

class customListView : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_custom_list_view)

        var listView = findViewById<ListView>(R.id.customListView)
        listView.adapter = myCustomAdapter(this)
    }

}

class myCustomAdapter(context: Context): BaseAdapter(){
    var myContext: Context
    init {
        myContext = context
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup):View{
        var inflaterX = LayoutInflater.from(myContext)
        var view = inflaterX.inflate(R.layout.custom_adapter, parent, false)
        var id = view.findViewById<TextView>(R.id.studentId)
        id.text = studentData.data.get(position).id.toString()

        var name = view.findViewById<TextView>(R.id.studentName)
        name.text = studentData.data.get(position).name

        var age = view.findViewById<TextView>(R.id.studentAge)
        age.text = studentData.data.get(position).age.toString()

        return view
    }
    override fun getItem(position: Int):Any{
        return ""
    }
    override fun getItemId(position: Int):Long{
        return position.toLong()
    }
    override fun getCount(): Int {
        return studentData.data.count()
    }
}