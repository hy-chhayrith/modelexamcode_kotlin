package com.example.chhayrithhy.modelexamcode.feature

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.widget.Toast

val dbName = "User"
val tableName = "UserInfo"

class DBConnector(context: Context): SQLiteOpenHelper(context, dbName, null, 1){
    override fun onCreate(db: SQLiteDatabase?){
        var createTable = "create table $tableName (name int, age int, email varchar(255), primary key(name));"

        db?.execSQL(createTable)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun insertData(data:userData){
        var db = this.writableDatabase
        var content_value = ContentValues()
        content_value.put("name", data.name)
        content_value.put("age", data.age)
        content_value.put("email", data.email)

        db.insert(tableName, null, content_value)

    }

    fun getData():ArrayList<userData>{
        var db = this.readableDatabase
        var data = db.rawQuery("select * from $tableName", null)
        var user = ArrayList<userData>()
        var u = userData()

        data.moveToFirst()
        do{
            u.name = data.getString(0).toString()
            u.age = data.getString(1).toString().toInt()
            u.email = data.getString(2).toString()
        }while(data.moveToNext())

        user.add(u)
        return user
    }
}